# License and Acknowledgement
The original BasicSR project is released under the Apache 2.0 license.  
Maintaining the license, we customize the model based on the previous work.

## References
1. [BasicSR](https://github.com/xinntao/BasicSR)
1. NIQE metric: the codes are translated from the [official MATLAB codes](http://live.ece.utexas.edu/research/quality/niqe_release.zip)
    > A. Mittal, R. Soundararajan and A. C. Bovik, "Making a Completely Blind Image Quality Analyzer", IEEE Signal Processing Letters, 2012.
1. FID metric: the codes are modified from [pytorch-fid](https://github.com/mseitzer/pytorch-fid) and [stylegan2-pytorch](https://github.com/rosinality/stylegan2-pytorch).
