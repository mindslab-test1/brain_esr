FROM nvcr.io/nvidia/pytorch:19.10-py3
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates curl sudo gnupg wget vim
EXPOSE 6008 6009

COPY . /root/brain_esr
WORKDIR /root/brain_esr
RUN pip install -r requirements.txt

RUN python setup.py develop --no_cuda_ext