# MINDsLab ESRGAN

This repository is heavily dependent on [`BasicSR`](https://github.com/xinntao/BasicSR).
Training code and its supporting materials are borrowed from them.
[LICENSE](./LICENSE) of the code remains same with the original author.

The novelty of this SR engine is that it trains with RRDBNet
and fine-tunes with the pretrained weight of corresponding model.

## How to set up

### Ready for Dataset

**CUDA Compatibility**  
This repository is based on `CUDA 10.1`, `pytorch==1.3`

### Pull (or Load) docker image
```shell script
# Pull
docker pull docker.maum.ai:443/brain/esr:v0.1.0
# Load
docker load -i brain_esr.tar
```

### Download basic dataset and soft-link to `datasets`
Please download HR dataset and LR dataset from the [DIV2K](https://data.vision.ee.ethz.ch/cvl/DIV2K/) site.  
Locate or soft-link these with following command.
Please modify `/path/to/dataset`
```shell script
ln -s /path/to/dataset ./datasets
ln -s /path/to/checkpoint ./experiments
ln -s /path/to/test ./test_image
```

### Run docker container with following command
Please modify `/path/to/dataset`
```shell script
docker run --ipc=host --rm -itd --gpus="all" -p 6008:6008 -v /path/to/checkpoint:/path/to/checkpoint -v /path/to/dataset:/path/to/dataset -v /path/to/test:/path/to/test --name brain_esr esr:v0.1.0
```

Entrance with following command
```shell script
docker exec -it brain_esr bash
```

### Training

First, train RRDBNet (about 7~10 days) and after that, train ESRGAN.

Make sure that in [`options/train/train_RRDBNet_PSNR_x4.yml`](./options/train/train_RRDBNet_PSNR_x4.yml) has exact db path.
```shell script
# Train RRDBNet
python train.py -opt options/train/train_RRDBNet_PSNR_x4.yml
```

Then, modify the `pretrain_network_g` in [`options/train/ESRGAN/train_ESRGAN_x4.yml`](./options/train/ESRGAN/train_ESRGAN_x4.yml) to the checkpoint of RRDBNet.
```shell script
# Train ESRGAN
python train.py -opt options/train/train_ESRGAN_x4.yml
```

To see the tensorboard,
```shell script
tensorboard --logdir ./tb_logger --port 6008 --bind_all
```

### Inference

Type
```shell script
python infer.py
```

Then the generated files are located in `test_image`, which is mounted with `/path/to/test`.